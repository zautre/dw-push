'use strict';

/**
 * Controller for all Web Push related functions.
 *
 * @module controllers/Push
 */

/* API Includes */
var PaymentMgr = require( 'dw/order/PaymentMgr' );
var Transaction = require( 'dw/system/Transaction' );
var Logger = require( 'dw/system/Logger' );
var ISML = require( 'dw/template/ISML' );

/* Script Modules */
var guard = require( '~/cartridge/scripts/guard' );

var helpers = {
		subscriptionUtils : require( '~/cartridge/scripts/subscriptionUtils.js' )
	};

/**
 * Saves the subscription object in the user's profile
 * 
 * @return {void}
 */
function subscribe()
{		
	try
	{
		helpers.subscriptionUtils.saveSubscription( request.httpParameterMap );	
	} 
	catch( e )
	{
		Logger.getLogger("[saveSubscription.ds]").error("Could Not Save Customer's subscription endpoint and/or key {0}", e.message);
		
	}
}

/**
 * Saves the subscription object in the user's profile
 * 
 * @return {void}
 */
function unsubscribe()
{		
	try
	{
		helpers.subscriptionUtils.saveSubscription( request.httpParameterMap );	
	} 
	catch( e )
	{
		Logger.getLogger("[removeSubscription.ds]").error("Could Not Remove Customer's subscription endpoint and/or key {0}", e.message);		
	}
}

/**
 * Include the push code
 * 
 * @return {void}
 */
function include()
{		
	ISML.renderTemplate( 'push/push_code' );
}

/**
 * Include the push manifest file
 * 
 * @return {void}
 */
function includeManifest()
{		
	ISML.renderTemplate( 'push/manifest_code' );
}

/*
 * Module exports
 */

/* Web exposed methods */

/** Saves the subscription object in the user's profile. */
exports.Subscribe = guard.ensure( [], subscribe );
/** Called to remove the subscription object from the user profile. */
exports.UnSubscribe = guard.ensure( [], unsubscribe );
/** Called to include the push code. Secure connection required! */
exports.Include = guard.ensure( ['https'], include );
/** Called to include the push manifest. */
exports.IncludeManifest = guard.ensure( [], includeManifest );