'use strict';

/**
*
*	Utility functions and Objects for handling the subscription object manipulations
*	
*	@module controllers/subscriptionUtils
*/

var Logger = require( 'dw/system/Logger' );
var Transaction = require( 'dw/system/Transaction' );
var log = Logger.getLogger( 'subscriptionUtils.ds' );

/**
 * Saves the subscription object in the user's profile
 * @param {dw.web.HttpParameterMap} httpParameterMap Current map of HTTP parameters.
 * 
 * @return {void}
 */
function saveSubscription( httpParameterMap )
{
	var pushSubscriptionObj = {};
	
	try
	{
		pushSubscriptionObj = _constructPushSubscriptionObj( httpParameterMap ); 
		
		_saveSubscriptionInProfile( pushSubscriptionObj );		
	} 
	catch( e )
	{
		log.error( "Could Not Save Customer's subscription endpoint and/or key. Exception: {1}", e.message);
	}	
}

/**
 * Saves the subscription object in the user's profile
 * @param {dw.web.HttpParameterMap} httpParameterMap Current map of HTTP parameters.
 * 
 * @return {void}
 */
function deleteSubscription( httpParameterMap )
{
	var pushSubscriptionObj = {};
	
	try
	{		
		pushSubscriptionObj = _constructPushSubscriptionObj( httpParameterMap ); 
		
		_removeSubscriptionFromProfile( pushSubscriptionObj );		
	} 
	catch( e )
	{
		log.error( "Could Not Save Customer's subscription endpoint and/or key. Exception: {1}", e.message);
	}	
}

/**
 * Constructs the subscription object
 * @param {dw.web.HttpParameterMap} httpParameterMap Current map of HTTP parameters.
 * 
 * @return {subsciptionObj} 
 */
function _constructPushSubscriptionObj( httpParameterMap )
{
	var pushSubscriptionObj = new Object();
	var endpoint = '';
	var p256dh = '';
	var auth = '';
	var browser = '';
	
	if( httpParameterMap.isParameterSubmitted( "endpoint" ) )
	{
		endpoint = httpParameterMap.get( "endpoint" );
	}
	if( httpParameterMap.isParameterSubmitted( "p256dh" ) )
	{
		p256dh = httpParameterMap.get( "p256dh" );
	}
	if( httpParameterMap.isParameterSubmitted( "auth" ) )
	{
		auth = httpParameterMap.get( "auth" );
	}
	if( httpParameterMap.isParameterSubmitted( "browser" ) )
	{
		browser = httpParameterMap.get( "browser" );
	}	
	
	pushSubscriptionObj.endpoint = endpoint.value;
	
	var keysObj = new Object();
	keysObj.auth = auth.value;
	keysObj.p256dh = p256dh.value;
	pushSubscriptionObj.keys = keysObj;
	pushSubscriptionObj.provider = browser.value;
	
	/**
	 * Push Subscription Object
	 * @typedef {Object} subsciptionObj
	 * @property{string} endpoint A USVString containing the endpoint associated with the push subscription.
	 * @property {string} auth An authentication secret, as described in Message Encryption for Web Push (https://tools.ietf.org/html/draft-ietf-webpush-encryption-08).
	 * @property {string} p256dh An Elliptic curve Diffie–Hellman public key on the P-256 curve (that is, the NIST secp256r1 elliptic curve).  The resulting key is an uncompressed point in ANSI X9.62 format.
	 * @property {string} browser user agent used for subscription
	 */
	return pushSubscriptionObj;
}

/**
 * Saves the push subscription object in customer's profile
 * @param {subsciptionObj} pushSubscriptionObj The push subscription object.
 * 
 * @return {void} 
 */
function _saveSubscriptionInProfile( pushSubscriptionObj )
{
	if (customer.anonymous)
	{
		var anonymousPushSubscription = new Array();
		
		let dataObject = CustomObjectMgr.createCustomObject( "push_anonymousSubscribers", customer.ID );	
		
		anonymousPushSubscription.push( JSON.stringify( pushSubscriptionObj ) );		
		
		Transaction.wrap( function()
		{
			dataObject.custom.subscriptionObject = anonymousPushSubscription;
		} );		
	} else 
	{
		var customerPushSubscription = new Array();
		
		for each( var subObj in customer.profile.custom[ "pushSubscription" ] )
		{
			//is subscribed already do not add to object but replace instead
			if( !JSON.parse( subObj ).provider.equals( pushSubscriptionObj.provider ) )
			{
				customerPushSubscription.push( subObj );
			}
		}
			
		customerPushSubscription.push( JSON.stringify( pushSubscriptionObj ) );	
		
		Transaction.wrap( function()
		{
			customer.profile.custom["pushSubscription"] = customerPushSubscription;
		} );
	}
	
	log.debug( "Added push subscription to profile: {0}", JSON.stringify( pushSubscriptionObj ) );
}

/**
 * Removes the push subscription object from customer's profile
 * @param {subsciptionObj} pushSubscriptionObj The push subscription object.
 * 
 * @return {boolean} result True if remove succeeds, otherwise false  
 */
function _removeSubscriptionFromProfile( pushSubscriptionObj )
{
	if( customer.anonymous )
	{	
		try 
		{
			let dataObject = CustomObjectMgr.queryCustomObject( "push_anonymousSubscribers", "custom.transactionUID LIKE {0}",  customer.ID);
			Transaction.wrap( function()
			{		
				CustomObjectMgr.remove( dataObject );
				
			} );			
		} catch ( e )
		{
			log.error( "Could not delete object {0}", e.message );
			return false;
		}	
	}
	else
	{
		var customerPushSubscription = new Array();
		
		for each( var subObj in customer.profile.custom[ "pushSubscription" ] )
		{
			//if subscribed already do not add to object, equivalent to delete
			if( !JSON.parse(subObj).provider.equals( pushSubscriptionObj.provider ) )
			{
				customerPushSubscription.push( subObj );
			}
		}		
		
		Transaction.wrap( function()
		{
			customer.profile.custom[ "pushSubscription" ] = customerPushSubscription;
		} );		
	}
	log.debug( "Removed push subscription from profile: {0}", JSON.stringify( pushSubscriptionObj ) );
	return true;
}

exports.saveSubscription = saveSubscription;
exports.deleteSubscription = deleteSubscription;