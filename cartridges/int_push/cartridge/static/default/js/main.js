'use strict';

var API_KEY = gcmAPIKey;

var isPushEnabled = false;

// This method handles the removal of subscriptionId
// in Chrome 44 by concatenating the subscription Id
// to the subscription endpoint
function endpointWorkaround(pushSubscription)
{
	// Make sure we only mess with GCM
	if (pushSubscription.endpoint.indexOf('https://android.googleapis.com/gcm/send') !== 0)
	{
		return pushSubscription.endpoint;
	}

	var mergedEndpoint = pushSubscription.endpoint;
	// Chrome 42 + 43 will not have the subscriptionId attached
	// to the endpoint.
	if (pushSubscription.subscriptionId &&
		pushSubscription.endpoint.indexOf(pushSubscription.subscriptionId) === -1)
	{
		// Handle version 42 where you have separate subId and Endpoint
		mergedEndpoint = pushSubscription.endpoint + '/' +
			pushSubscription.subscriptionId;
	}
	return mergedEndpoint;
}

function sendSubscriptionToServer(subscription)
{
	var xmlDoc = getXmlDoc();
	xmlDoc.open('POST', subscribeURL, true);
	xmlDoc.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	var subscriptionJSON = JSON.parse(JSON.stringify(subscription));
	var browser = detectBrowser();
	xmlDoc.send("endpoint=" + subscriptionJSON.endpoint + "&" + "p256dh=" + subscriptionJSON.keys.p256dh + "&" + "auth=" + subscriptionJSON.keys.auth + "&" + "browser=" + browser);
	xmlDoc.onreadystatechange = function ()
	{
		if (xmlDoc.readyState === XMLHttpRequest.DONE && xmlDoc.status === 200)
		{
			console.log('Successfuly sent subscription object to DW server  ');
		}
		else
		{
			console.log('DW server response code not 200');
		}
	}

	// For compatibly of Chrome 43, get the endpoint via
	// endpointWorkaround(subscription)
	var mergedEndpoint = endpointWorkaround(subscription);
}

function sendUnSubscriptionToServer(subscription)
{
	var xmlDoc = getXmlDoc();
	xmlDoc.open('POST', unSubscribeURL, true);
	xmlDoc.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	var subscriptionJSON = JSON.parse(JSON.stringify(subscription));
	var browser = detectBrowser();
	xmlDoc.send("endpoint=" + subscriptionJSON.endpoint + "&" + "p256dh=" + subscriptionJSON.keys.p256dh + "&" + "auth=" + subscriptionJSON.keys.auth + "&" + "browser=" + browser);
	xmlDoc.onreadystatechange = function ()
	{
		if (xmlDoc.readyState === XMLHttpRequest.DONE && xmlDoc.status === 200)
		{
			console.log('Successfuly sent un subscription object to DW server  ');
		}
		else
		{
			console.log('DW server response code not 200');
		}
	}

	// For compatibly of Chrome 43, get the endpoint via
	// endpointWorkaround(subscription)
	var mergedEndpoint = endpointWorkaround(subscription);
}

function unsubscribe()
{
	var pushButton = document.querySelector('.js-push-button');
	pushButton.disabled = true;

	navigator.serviceWorker.ready.then(function (serviceWorkerRegistration)
	{
		// To unsubscribe from push messaging, you need get the
		// subcription object, which you can call unsubscribe() on.
		serviceWorkerRegistration.pushManager.getSubscription().then(
			function (pushSubscription)
			{
				// Check we have a subscription to unsubscribe
				if (!pushSubscription)
				{
					// No subscription object, so set the state
					// to allow the user to subscribe to push
					isPushEnabled = false;
					pushButton.disabled = false;
					pushButton.textContent = 'Enable Push Messages';
					return;
				}

				sendUnSubscriptionToServer(pushSubscription);

				// We have a subcription, so call unsubscribe on it
				pushSubscription.unsubscribe().then(function ()
				{
					pushButton.disabled = false;
					pushButton.textContent = 'Enable Push Messages';
					isPushEnabled = false;
				}).catch(function (e)
				{
					// We failed to unsubscribe, this can lead to
					// an unusual state, so may be best to remove
					// the subscription id from your data store and
					// inform the user that you disabled push

					window.Demo.debug.log('Unsubscription error: ', e);
					pushButton.disabled = false;
				});
			}).catch(function (e)
		{
			window.Demo.debug.log('Error thrown while unsubscribing from ' +
				'push messaging.', e);
		});
	});
}

function urlBase64ToUint8Array(base64String)
{
	const padding = '='.repeat((4 - base64String.length % 4) % 4);
	const base64 = (base64String + padding)
		.replace(/\-/g, '+')
		.replace(/_/g, '/');

	const rawData = window.atob(base64);
	const outputArray = new Uint8Array(rawData.length);

	for (let i = 0; i < rawData.length; ++i)
	{
		outputArray[i] = rawData.charCodeAt(i);
	}
	return outputArray;
}

function subscribe()
{
	// Disable the button so it can't be changed while
	// we process the permission request
	var pushButton = document.querySelector('.js-push-button');
	pushButton.disabled = true;

	navigator.serviceWorker.ready.then(function (serviceWorkerRegistration)
	{
		serviceWorkerRegistration.pushManager.subscribe(
			{
				userVisibleOnly: true,
				applicationServerKey: urlBase64ToUint8Array(vapidPublicKey)
			}).then(function (subscription)
			{
				// The subscription was successful
				isPushEnabled = true;
				pushButton.textContent = 'Disable Push Messages';
				pushButton.disabled = false;

				// TODO: Send the subscription subscription.endpoint
				// to your server and save it to send a push message
				// at a later date
				return sendSubscriptionToServer(subscription);
			})
			.catch(function (e)
			{
				if (Notification.permission === 'denied')
				{
					// The user denied the notification permission which
					// means we failed to subscribe and the user will need
					// to manually change the notification permission to
					// subscribe to push messages
					window.Demo.debug.log('Permission for Notifications was denied');
					pushButton.disabled = true;
				}
				else
				{
					// A problem occurred with the subscription, this can
					// often be down to an issue or lack of the gcm_sender_id
					// and / or gcm_user_visible_only
					window.Demo.debug.log('Unable to subscribe to push.', e);
					pushButton.disabled = false;
					pushButton.textContent = 'Enable Push Messages';
				}
			});
	});
}

// Once the service worker is registered set the initial state
function initialiseState()
{
	// Are Notifications supported in the service worker?
	if (!('showNotification' in ServiceWorkerRegistration.prototype))
	{
		window.Demo.debug.log('Notifications aren\'t supported.');
		return;
	}

	// Check the current Notification permission.
	// If its denied, it's a permanent block until the
	// user changes the permission
	if (Notification.permission === 'denied')
	{
		window.Demo.debug.log('The user has blocked notifications.');
		return;
	}

	// Check if push messaging is supported
	if (!('PushManager' in window))
	{
		window.Demo.debug.log('Push messaging isn\'t supported.');
		return;
	}

	// We need the service worker registration to check for a subscription
	navigator.serviceWorker.ready.then(function (serviceWorkerRegistration)
	{
		// Do we already have a push message subscription?
		serviceWorkerRegistration.pushManager.getSubscription()
			.then(function (subscription)
			{
				// Enable any UI which subscribes / unsubscribes from
				// push messages.
				var pushButton = document.querySelector('.js-push-button');
				pushButton.disabled = false;

				if (!subscription)
				{
					// We aren’t subscribed to push, so set UI
					// to allow the user to enable push
					return;
				}

				// Keep your server in sync with the latest subscription
				sendSubscriptionToServer(subscription);

				// Set your UI to show they have subscribed for
				// push messages
				pushButton.textContent = 'Disable Push Messages';
				isPushEnabled = true;
			})
			.catch(function (err)
			{
				window.Demo.debug.log('Error during getSubscription()', err);
			});
	});
}

window.addEventListener('load', function ()
{
	var pushButton = document.querySelector('.js-push-button');
	pushButton.addEventListener('click', function ()
	{
		if (isPushEnabled)
		{
			unsubscribe();
		}
		else
		{
			subscribe();
		}
	});

	// Check that service workers are supported, if so, progressively
	// enhance and add push messaging support, otherwise continue without it.
	if ('serviceWorker' in navigator)
	{
		navigator.serviceWorker.register(swURL)
			.then(initialiseState);
	}
	else
	{
		window.Demo.debug.log('Service workers aren\'t supported in this browser.');
	}
});

function getXmlDoc()
{
	var xmlDoc;

	if (window.XMLHttpRequest)
	{
		// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlDoc = new XMLHttpRequest();
	}
	else
	{
		// code for IE6, IE5
		xmlDoc = new ActiveXObject("Microsoft.XMLHTTP");
	}

	return xmlDoc;
}
/**
 * JavaScript Client Detection
 * (C) viazenetti GmbH (Christian Ludwig)
 * changed according zaUtre needs
 */
function detectBrowser() {
    {
        var unknown = '-';
        // browser
        var nVer = navigator.appVersion;
        var nAgt = navigator.userAgent;
        var browser = navigator.appName;
        var version = '' + parseFloat(navigator.appVersion);
        var majorVersion = parseInt(navigator.appVersion, 10);
        var nameOffset, verOffset, ix;

        // Opera
        if ((verOffset = nAgt.indexOf('Opera')) != -1) {
            browser = 'Opera';
            version = nAgt.substring(verOffset + 6);
            if ((verOffset = nAgt.indexOf('Version')) != -1) {
                version = nAgt.substring(verOffset + 8);
            }
        }
        // Opera Next
        if ((verOffset = nAgt.indexOf('OPR')) != -1) {
            browser = 'Opera';
            version = nAgt.substring(verOffset + 4);
        }
        // Edge
        else if ((verOffset = nAgt.indexOf('Edge')) != -1) {
            browser = 'Microsoft Edge';
            version = nAgt.substring(verOffset + 5);
        }
        // MSIE
        else if ((verOffset = nAgt.indexOf('MSIE')) != -1) {
            browser = 'Microsoft Internet Explorer';
            version = nAgt.substring(verOffset + 5);
        }
        // Chrome
        else if ((verOffset = nAgt.indexOf('Chrome')) != -1) {
            browser = 'Chrome';
            version = nAgt.substring(verOffset + 7);
        }
        // Safari
        else if ((verOffset = nAgt.indexOf('Safari')) != -1) {
            browser = 'Safari';
            version = nAgt.substring(verOffset + 7);
            if ((verOffset = nAgt.indexOf('Version')) != -1) {
                version = nAgt.substring(verOffset + 8);
            }
        }
        // Firefox
        else if ((verOffset = nAgt.indexOf('Firefox')) != -1) {
            browser = 'Firefox';
            version = nAgt.substring(verOffset + 8);
        }
        // MSIE 11+
        else if (nAgt.indexOf('Trident/') != -1) {
            browser = 'Microsoft Internet Explorer';
            version = nAgt.substring(nAgt.indexOf('rv:') + 3);
        }
        // Other browsers
        else if ((nameOffset = nAgt.lastIndexOf(' ') + 1) < (verOffset = nAgt.lastIndexOf('/'))) {
            browser = nAgt.substring(nameOffset, verOffset);
            version = nAgt.substring(verOffset + 1);
            if (browser.toLowerCase() == browser.toUpperCase()) {
                browser = navigator.appName;
            }
        }
        // trim the version string
        if ((ix = version.indexOf(';')) != -1) version = version.substring(0, ix);
        if ((ix = version.indexOf(' ')) != -1) version = version.substring(0, ix);
        if ((ix = version.indexOf(')')) != -1) version = version.substring(0, ix);

        majorVersion = parseInt('' + version, 10);
        if (isNaN(majorVersion)) {
            version = '' + parseFloat(navigator.appVersion);
            majorVersion = parseInt(navigator.appVersion, 10);
        }

        // mobile version
        var mobile = /Mobile|mini|Fennec|Android|iP(ad|od|hone)/.test(nVer);
        if (mobile) {
            mobile = "Mobile Version";
        }
        else {
            mobile = "Desktop Version";
        }

        // cookie
        var cookieEnabled = (navigator.cookieEnabled) ? true : false;

        if (typeof navigator.cookieEnabled == 'undefined' && !cookieEnabled) {
            document.cookie = 'testcookie';
            cookieEnabled = (document.cookie.indexOf('testcookie') != -1) ? true : false;
        }

        // system
        var os = unknown;
        var clientStrings = [
            {s:'Windows 10', r:/(Windows 10.0|Windows NT 10.0)/},
            {s:'Windows 8.1', r:/(Windows 8.1|Windows NT 6.3)/},
            {s:'Windows 8', r:/(Windows 8|Windows NT 6.2)/},
            {s:'Windows 7', r:/(Windows 7|Windows NT 6.1)/},
            {s:'Windows Vista', r:/Windows NT 6.0/},
            {s:'Windows Server 2003', r:/Windows NT 5.2/},
            {s:'Windows XP', r:/(Windows NT 5.1|Windows XP)/},
            {s:'Windows 2000', r:/(Windows NT 5.0|Windows 2000)/},
            {s:'Windows ME', r:/(Win 9x 4.90|Windows ME)/},
            {s:'Windows 98', r:/(Windows 98|Win98)/},
            {s:'Windows 95', r:/(Windows 95|Win95|Windows_95)/},
            {s:'Windows NT 4.0', r:/(Windows NT 4.0|WinNT4.0|WinNT|Windows NT)/},
            {s:'Windows CE', r:/Windows CE/},
            {s:'Windows 3.11', r:/Win16/},
            {s:'Android', r:/Android/},
            {s:'Open BSD', r:/OpenBSD/},
            {s:'Sun OS', r:/SunOS/},
            {s:'Linux', r:/(Linux|X11)/},
            {s:'iOS', r:/(iPhone|iPad|iPod)/},
            {s:'Mac OS X', r:/Mac OS X/},
            {s:'Mac OS', r:/(MacPPC|MacIntel|Mac_PowerPC|Macintosh)/},
            {s:'QNX', r:/QNX/},
            {s:'UNIX', r:/UNIX/},
            {s:'BeOS', r:/BeOS/},
            {s:'OS/2', r:/OS\/2/},
            {s:'Search Bot', r:/(nuhk|Googlebot|Yammybot|Openbot|Slurp|MSNBot|Ask Jeeves\/Teoma|ia_archiver)/}
        ];
        for (var id in clientStrings) {
            var cs = clientStrings[id];
            if (cs.r.test(nAgt)) {
                os = cs.s;
                break;
            }
        }

        var osVersion = unknown;

        if (/Windows/.test(os)) {
            osVersion = /Windows (.*)/.exec(os)[1];
            os = 'Windows';
        }

        switch (os) {
            case 'Mac OS X':
                osVersion = /Mac OS X (10[\.\_\d]+)/.exec(nAgt)[1];
                break;

            case 'Android':
                osVersion = /Android ([\.\_\d]+)/.exec(nAgt)[1];
                break;

            case 'iOS':
                osVersion = /OS (\d+)_(\d+)_?(\d+)?/.exec(nVer);
                osVersion = osVersion[1] + '.' + osVersion[2] + '.' + (osVersion[3] | 0);
                break;
        }
        
    }

    let provider = browser + ' ' + version + ' / ' + os + '  ' + osVersion + ' / ' + mobile;
    return provider; 
}