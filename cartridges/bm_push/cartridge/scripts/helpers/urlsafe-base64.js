'use strict';
/**
 * This module provides helper functions for base64 URL save string encode.
 *
 * @module helpers/urlsafe-base64
 */

let Encoding = require( 'dw/crypto/Encoding' );
let Bytes = require( 'dw/util/Bytes' );

module.exports = {
	encode 		: encode,
	decode		: decode,
	toURLSave	: toURLSave,
	toURLUnSave : toURLUnSave,
	validate	: validate 
};

/**
 * return an encoded String as URL Safe Base64
 *
 * Note: This function encodes to the RFC 4648 Spec where '+' is encoded
 *       as '-' and '/' is encoded as '_'. The padding character '=' is
 *       removed.
 *
 * @param 	{dw.util.Bytes} bytesArray Bytes array to encode
 * @return 	{String} 		base64 URL save encoded string
 */
function encode( bytesArray )
{

	return Encoding.toBase64( bytesArray )
		.replace( /\+/g, '-' ) // Convert '+' to '-'
		.replace( /\//g, '_' ) // Convert '/' to '_'
		.replace( new RegExp( '=+$' ), '' ); // Remove ending '='

}

/**
 * return an Base64 encoded String as URL Safe
 *
 * Note: This function encodes to the RFC 4648 Spec where '+' is encoded
 *       as '-' and '/' is encoded as '_'. The padding character '=' is
 *       removed.
 *
 * @param 	{string} string base64 non URL save encoded string
 * @return 	{String} 		base64 URL save encoded string
 */
function toURLSave( string )
{

	return string
		.replace( /\+/g, '-' ) // Convert '+' to '-'
		.replace( /\//g, '_' ) // Convert '/' to '_'
		.replace( new RegExp( '=+$' ), '' ); // Remove ending '='

}

/**
 * returns original none-url-save Base64 encoded String 
 *
 *
 * @param 	{string} base64string 	base64 URL save encoded string
 * @return 	{String} 				base64 none URL save encoded string
 */
function toURLUnSave( base64string )
{
	let output = base64string;
	// Add removed at end '='
	output += Array( 5 - base64string.length % 4 ).join( '=' );

	output = output
		.replace( /\-/g, '+' ) // Convert '-' to '+'
		.replace( /\_/g, '/' ); // Convert '_' to '/'
	
	return output;	
}


/**
 * Return an decoded URL Safe Base64 as String
 *
 * @param {String} base64string URL save string to decode
 * @return {dw.util.Bytes} decoded bytes array
 */
function decode( base64string )
{
	let output = base64string;
	// Add removed at end '='
	output += Array( 5 - base64string.length % 4 ).join( '=' );

	output = output
		.replace( /\-/g, '+' ) // Convert '-' to '+'
		.replace( /\_/g, '/' ); // Convert '_' to '/'

	return Encoding.fromBase64( output );

}

/**
 * Validates a string if it is URL Safe Base64 encoded.
 *
 * @param {string} base64 base64 URL save encoded string
 * @return {boolean} true if string is URL Safe Base64 encoded, otherwise false
 */
function validate( base64 )
{

	return /^[A-Za-z0-9\-_]+$/.test( base64 );

}