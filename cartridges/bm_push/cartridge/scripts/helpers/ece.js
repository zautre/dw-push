'use strict';
/**
 * Encrypted content coding
 *
 * === Note about versions ===
 *
 * This code supports multiple versions of the draft.  This is selected using
 * the |version| parameter.
 *
 * aes128gcm: The most recent version, the salt, record size and key identifier
 *    are included in a header that is part of the encrypted content coding.
 *
 * aesgcm: The version that is widely deployed with WebPush (as of 2016-11).
 *    This version is selected by default, unless you specify a |padSize| of 1.
 *
 * aesgcm128: This version is old and will be removed in an upcoming release.
 *     This version is selected by providing a |padSize| parameter of 1.
 * 
 * @module helpers/ece
 **/

let Encoding = require( 'dw/crypto/Encoding' );
let Logger = require( 'dw/system/Logger' );
let Mac = require( 'dw/crypto/Mac' );
let SecureRandom = require( 'dw/crypto/SecureRandom' );
let Bytes = require( 'dw/util/Bytes' );
let StringUtils = require( 'dw/util/StringUtils' );

let sjcl = require( '~/cartridge/scripts/sjcl-1.0.6/sjcl-1.0.6.js' );
let urlBase64 = require( '~/cartridge/scripts/helpers/urlsafe-base64.js' );

module.exports = {
	encrypt: encrypt
};

let PAD_SIZE = {
	'aes128gcm': 2,
	'aesgcm': 2,
	'aesgcm128': 1
};
let TAG_LENGTH = 16;
let KEY_LENGTH = 16;
let NONCE_LENGTH = 12;
let SHA_256_LENGTH = 32;

/**
 * RFC 5869 HMAC-Hash function using SHA-256 as the message digest algorithm
 *
 * @param  {string} key		base64 encoded key.
 * @param  {string} input	base64 encoded input (or message)
 * 
 * @return {string}    		base64 encoded string The hash value for the passed string input using the passed secret key
 */
function HMAC_hash( key, input )
{
	let keyBits = sjcl.codec.base64.toBits( urlBase64.toURLUnSave( key ) );
	let inputBits = sjcl.codec.base64.toBits( urlBase64.toURLUnSave( input ) );
	
	let hmac = new sjcl.misc.hmac( keyBits, sjcl.hash.sha256 );
	let output = hmac.encrypt( inputBits );
	output = sjcl.codec.base64.fromBits( output );
	
	return output;	
}

/**
 * RFC 5869 Extract SHA-256
 *
 * @param  {string} salt	base64 encoded string salt value (a non-secret random value).
 * @param  {string} ikm	    base64 encoded string input keying material
 * 
 * @return {string} PRK  	base64 encoded string A pseudorandom key (of HashLen octets)
 */
function HKDF_extract( salt, ikm )
{
	return HMAC_hash( salt, ikm );
}

/**
 * RFC 5869 Expand using SHA-256
 *
 * @param  {string} 	prk		base64 encoded string A pseudorandom key of at least HashLen ( HashLen denotes the length of the hash function output in octets) octets (usually, the output from the extract step)
 * @param  {string} 	inf		base64 encoded string An optional context and application specific information (can be a zero-length string)
 * @param  {string} 	L		length of output keying material in octets (<= 255*HashLen)
 * 
 * @return {string} 	okm   	base64 encoded string Output keying material (of L octets).
 */
function HKDF_expand( prk, inf, L )
{
	
	let n = Math.ceil( L / 32 );
	let t = new Array( n + 1 );
	let infoBits = sjcl.codec.base64.toBits( inf );
	let okm = sjcl.codec.utf8String.toBits( "" ); // Output Keying Material
	
	t[0] = sjcl.codec.utf8String.toBits( "" );
	
	for ( i = 1; i <= n; ++i ) 
	{
		let input = "";
		input = sjcl.bitArray.concat( t[i - 1], infoBits );
		input = sjcl.bitArray.concat( input, sjcl.codec.hex.toBits( "0x0" + i ) ); //that would break for i above 9, but there is no real need for such large okm
		
		t[i] = sjcl.codec.base64.toBits( HMAC_hash( prk, sjcl.codec.base64.fromBits( input ) ) );
		
		okm = sjcl.bitArray.concat( okm, t[i] );
	}	
	okm = sjcl.bitArray.bitSlice( okm, 0, L*8 );	
	
	return sjcl.codec.base64.fromBits( okm );
}

/**
 * RFC 5869 HMAC-based Key Derivation Function
 *
 * @param  {string} salt	base64 encoded string optional salt value (a non-secret random value) if not provided, it is set to a string of HashLen zeros.
 * @param  {string} ikm		base64 encoded string input keying material
 * @param  {string} inf		base64 encoded string An optional context and application specific information (can be a zero-length string)
 * @param  {string} len		length of output keying material in octets (<= 255*HashLen)
 * 	
 * @return {dw.util.Bytes}			derived key    
 */
function HKDF( salt, ikm, inf, len )
{
	return HKDF_expand( HKDF_extract( salt, ikm ), inf, len );
}

/**
* Compose the info string
*
* @param  {string} 	base 		base string
* @param  {string} 	context 	base64 encoded context string
* 
* @return {string}     base64 encoded info string
*/
function info( base, context )
{
	let stringPrefixBits = sjcl.codec.utf8String.toBits( 'Content-Encoding: ' + base );
	stringPrefixBits = sjcl.bitArray.concat( stringPrefixBits, sjcl.codec.utf8String.toBits( '\x00' ) );
	let contextBits = sjcl.codec.base64.toBits( context );
	let output = sjcl.bitArray.concat( stringPrefixBits, contextBits );
	
	return sjcl.codec.base64.fromBits( output );
}

/**
 * Compose the context string, consisting of the following:
 * A string comprised of ‘P-256’
 * Followed by a NULL (“\x00”)
 * Followed by a network ordered, two octet integer of the length of the decoded receiver key
 * Followed by the decoded receiver key
 * Followed by a networked ordered, two octet integer of the length of the public half of the sender key
 * Followed by the public half of the sender key.. 
 *
 * @param  {string} 	senderKey 		base64 encoded sender key
 * @param  {string} 	receiverKey 	base64 encoded reciver key
 * @param  {string} 	prefix 			prefix string usually ‘P-256’
 * 
 * @return {string}     base64 encoded context string
 */
function composeContext( senderKey, receiverKey, prefix )
{
	let context = ""; 
	let receiverkey = urlBase64.toURLUnSave( receiverKey );
	let senderkey = urlBase64.toURLUnSave( senderKey );
	
	let receiverKeyBits = sjcl.codec.base64.toBits( receiverkey );
	let receiverKeyLength = sjcl.codec.hex.toBits( "00" + getByteLengthHex( receiverkey ) );
	let senderKeyBits = sjcl.codec.base64.toBits( senderkey );
	let senderKeyLength = sjcl.codec.hex.toBits( "00" + getByteLengthHex( senderkey ) );
	
	let contextComponentBits = sjcl.bitArray.concat( receiverKeyLength, receiverKeyBits );
	contextComponentBits = sjcl.bitArray.concat( contextComponentBits, senderKeyLength );
	contextComponentBits = sjcl.bitArray.concat( contextComponentBits, senderKeyBits );
	
	let prefixBits = sjcl.codec.utf8String.toBits( prefix ); 
	prefixBits = sjcl.bitArray.concat ( prefixBits, sjcl.codec.hex.toBits( "00" ) );
	
	context = sjcl.bitArray.concat( prefixBits, contextComponentBits );
	
	return sjcl.codec.base64.fromBits( context );
}

/**
 * Return byte length as a hexadecimal string. 
 *
 * @param  {string} string Base64 encoded string.
 * 
 * @return {string} Hexadecimal string with the byte length.
 */
function getByteLengthHex( string )
{
	let byteLength = sjcl.bitArray.bitLength( sjcl.codec.base64.toBits( string ) )/8;
	
	//return byte length as hexadecimal string
	return byteLength.toString( 16 );
}

/**
 * Derive context and shared key. 
 *
 * @param  {Object} [header]	parameters.
 * 
 * @return {SecretAndContext}   Object containing shared secret and context.
 */
function extractDH( header )
{
	let key = header.privateKey;
	let senderPubKey = "";
	let receiverPubKey = "";
	let output = "";
	let sharedSecret = "";
	let context = "";
	
	if( !key )
	{
		if ( !header.keymap || !header.keyid || !header.keymap[header.keyid] )
		{
			Logger.getLogger( "[ece.js]" ).error( 'No known DH key for {0}', header.keyid );
		}
		key = header.keymap[header.keyid];
	}
	if( !header.keylabels[header.keyid] )
	{
		Logger.getLogger( "[ece.js]" ).error( 'No known DH key label for {0}', header.keyid );
	}	
	senderPubKey = key.getPublicKey();
	receiverPubKey = header.dh;
	
	sharedSecret = computeSecret( key, header.dh );
	Logger.getLogger( "[ece.js]" ).debug( 'Shared Secret: {0}', sharedSecret );
	
	context = composeContext( senderPubKey, receiverPubKey, header.keylabels[header.keyid] );
	
	/**
	 * @typedef {Object}  SecretAndContext 
	 * @property {string} Base64 encoded shared secret
	 * @property {string} Base64 encoded context
	 */
	output = {
		secret: sharedSecret,
		context: context		
	};

	return output;
}

/**
 * Computes the shared secret using publicKey as the other party's public key and returns the computed shared secret. 
 *
 * @param  {Object} [key]		Private key object.
 * @param  {string} [publicKey] The p256dh key from the subscription object. Base64 encoded URL save string
 * 
 * @return {string}             The shared secret as base64 encoded string.
 */
function computeSecret( key, publicKey )
{
	// Unserialize private key:
	let sec = new sjcl.ecc.elGamal.secretKey(
	    sjcl.ecc.curves.c256,
	    sjcl.ecc.curves.c256.field.fromBits( sjcl.codec.base64.toBits( key.getPrivateKey() ) )
	);
	//p256dh (publicKey) is URL save, turn it back to original
	let publickey = urlBase64.toURLUnSave( publicKey );
	
	//p256dh (publicKey) is 65 bytes, strip the fixed leading 1 Byte in order to use it with SJCL
	let publicKeySegment = Encoding.fromBase64( publickey ).bytesAt( 1,64 );
	let publicKeyBase64 = Encoding.toBase64( publicKeySegment );
	
	// Unserialize the public key:
	let pub = new sjcl.ecc.basicKey.publicKey( sjcl.ecc.curves.c256, sjcl.codec.base64.toBits( publicKeyBase64 ) );
	
	let sharedSecretKeyBitArray = sec.dhJavaEc( pub );	
	let sharedSecret = sjcl.codec.base64.fromBits( sharedSecretKeyBitArray );
	
	Logger.getLogger( "[ece.js]" ).debug( 'Computed Secret: {0}', sharedSecret );
	
	//return sharedSectret as base64 string
	return sharedSecret;
}

/**
 * Extract secret and context 
 *
 * @param  {Object} [header]	parameters.
 * 
 * @return {SecretAndContext}   Object containing shared secret and context.
 */
function extractSecretAndContext( header )
{
	let result = {
		secret: null,
		context: ""
	};	
	
	if( header.key )
	{
		result.secret = header.key;
		if( result.secret.length !== KEY_LENGTH )
		{
			Logger.getLogger( "[ece.js]" ).error( 'An explicit key must be {0} bytes', KEY_LENGTH );
		}
	}
	else if( header.dh )
	{
		result = extractDH( header );
	}
	else if( typeof header.keyid !== undefined )
	{
		result.secret = header.keymap[header.keyid];
	}
	if( !result.secret )
	{
		Logger.getLogger( "[ece.js]" ).error( 'Unable to determine key' );
	}	
	if( header.authSecret )
	{
		result.secret = HKDF( header.authSecret, result.secret, info( 'auth', "" ), SHA_256_LENGTH );
		Logger.getLogger( "[ece.js]" ).debug( 'AuthSecret: {0}', result.secret );
	}
	
	return result;
}

/**
 * Extract secret for aes128gcm
 *
 * @param  {Object} [header]	parameters.
 * 
 * @return {SecretAndContext}   Object containing shared secret and context.
 */
function extractSecret( header )
{
	let remotePubKey = "";
	let senderPubKey = "";
	let receiverPubKey = "";
	let secret = "";
	let key = "";
	
	if( header.key )
	{
		if( header.key.length !== KEY_LENGTH )
		{
			Logger.getLogger( "[ece.js]" ).error( 'An explicit key must be {0} bytes', KEY_LENGTH );
		}
		return header.key;
	}

	if ( !header.privateKey )
	{
		// Lookup based on keyid
		key = header.keymap && header.keymap[header.keyid];
		
		if ( !key )
		{
			Logger.getLogger( "[ece.js]" ).error( 'No saved key (keyid:"{0}")', header.keyid );
		}
		return key;
	}

	if ( !header.authSecret )
	{
		Logger.getLogger( "[ece.js]" ).error( 'No authentication secret for webpush' );
	}

	senderPubKey = header.privateKey.getPublicKey();
	remotePubKey = receiverPubKey = header.dh;
	
	secret = HKDF( header.authSecret, computeSecret( header.privateKey, remotePubKey ),	'WebPush: info\0' + receiverPubKey + senderPubKey, SHA_256_LENGTH );

	return secret;
}

/**
 * Derive encryption key and encryption nonce 
 *
 * @param  {Object} [header]	parameters.
 * 
 * @return {KeyNonce}     	Object containing encryption key and encryption nonce both as base64 encoded strings.
 */
function deriveKeyAndNonce( header )
{
	let keyInfo = "";
	let nonceInfo = "";
	let secret = "";
	let prk = "";
	
	if( !header.salt )
	{
		Logger.getLogger( "[ece.js]" ).error( 'must include a salt parameter' );
	}
	if( header.version === 'aesgcm128' )
	{
		// really old
		keyInfo = 'Content-Encoding: aesgcm128';
		nonceInfo = 'Content-Encoding: nonce';
		secret = extractSecretAndContext( header ).secret;
	}
	else if( header.version === 'aesgcm' )
	{
		// old
		let s = extractSecretAndContext( header );
		keyInfo = info( 'aesgcm', s.context );
		nonceInfo = info( 'nonce', s.context );
		secret = s.secret;
	}
	else if( header.version === 'aes128gcm' )
	{
		// latest
		keyInfo = 'Content-Encoding: aes128gcm\x00';
		nonceInfo = 'Content-Encoding: nonce\x00';
		secret = extractSecret( header );
	}
	else
	{
		Logger.getLogger( "[ece.js]" ).error( 'Unable to set context for mode {0}', params.version );
	}
	prk = HKDF_extract( header.salt, secret );	
	
	/**
	 * @typedef {Object}  KeyNonce 
	 * @property {string} key   base64 encoded encryption key
	 * @property {string} nonce base64 encoded encryption nonce
	 */
	let result = {
		key: HKDF_expand( prk, keyInfo, KEY_LENGTH ),
		nonce: HKDF_expand( prk, nonceInfo, NONCE_LENGTH )
	};
	Logger.getLogger( "[ece.js]" ).debug( 'Derived key: {0}', result.key );
	Logger.getLogger( "[ece.js]" ).debug( 'Derived nonce: {0}', result.nonce );
	
	return result;
}

/**
 * Parse input arguments 
 *
 * @param  {Object} params input parameters.
 * 
 * @return {Object} Parsed parameters.
 */
function parseParams( params )
{
	let header = {};

	header.version = _getVersion( params );
	header.salt = _getSalt( params );
	header.keyid = params.keyid;
	
	if( params.key )
	{
		header.key = Encoding.fromBase64( params.key );
	}
	else
	{
		header.privateKey = params.privateKey;
		if( !header.privateKey )
		{
			header.keymap = params.keymap;
		}
		if( header.version !== 'aes128gcm' )
		{
			header.keylabels = params.keylabels;
		}
		if( params.dh )
		{
			header.dh = params.dh;
		}
	}
	header.authSecret = params.authSecret;
	
	return header;
}

/**
 * Get the web-push draft version
 *
 * @param  {Object} params input parameters.
 * @private
 * @return {string} draft version.
 */
function _getVersion( params )
{
	if( params.version )
	{
		return params.version;
	}

	return ( params.padSize === 1 ) ? 'aesgcm128' : 'aesgcm';
}

/**
 * Get salt
 *
 * @param  {Object} params input parameters.
 * @private
 * @return {string} salt base64 string
 */
function _getSalt( params )
{
	if( params.salt )
	{
		if ( urlBase64.decode( params.salt ).length !== KEY_LENGTH )
		{
			Logger.getLogger( "[ece.js]" ).error( 'The salt parameter must be {0} bytes', KEY_LENGTH );
		}
		
		return params.salt;				
	}
	let random = new SecureRandom();
	
	return urlBase64.encode( random.nextBytes( KEY_LENGTH ) );
}

/**
 * Encrypt message text
 *
 * @param  {string} [key]		Base64 encoded string Encryption key 
 * @param  {string} [nonce]		Base64 encoded string Encryption nonce.
 * @param  {string} [plaintext]	message plain text content.
 * 
 * @return {string} [encrypted] hex encoded string of the encrypted message   
 */
function encryptRecord( key, nonce, plaintext )
{
	let keyBits = sjcl.codec.base64.toBits( key );
	let prp = new sjcl.cipher.aes( keyBits );
	
	let adata = new Array( 0 );
	
	let plaintextBits = sjcl.codec.utf8String.toBits( "\x00\x00" + plaintext ); //following the webpush specification we need to prefix "\x00\x00" to the plain text message
	let nonceBits = sjcl.codec.base64.toBits( nonce );
	
	let etext = sjcl.codec.hex.fromBits( sjcl.mode.gcm.encrypt( prp, plaintextBits, nonceBits, adata ) );
	
	Logger.getLogger( "[ece.js]" ).debug( 'Finished message encryption. Message: {0}', plaintext );
	
	return etext;
}

function writeHeader( header )
{
	let keyid = header.keyid || [];
	let output = "";
	
	if( keyid.length > 255 )
	{
		Logger.getLogger( "[ece.js]" ).error( 'keyid is too large' );
	}	
	output = header.salt.toString() + keyid.length.toString() + keyid;

	return output;
}

/**
 * Encrypt some bytes.  This uses the parameters to determine the key and block size, which are described in the draft.
 *
 * |params.version| contains the version of encoding to use: aes128gcm is the latest, but aesgcm and aesgcm128 are also accepted (though the latter two might
 * disappear in a future release).  If omitted, assume aesgcm, unless |params.padSize| is set to 1, which means aesgcm128.
 *
 * If |params.key| is specified, that value is used as the key.
 *
 * If |params.keyid| is specified without |params.dh|, the keyid value is used to lookup the |params.keymap| for a buffer containing the key.
 *
 * For Diffie-Hellman (WebPush), |params.dh| includes the public key of the receiver.  |params.privateKey| is used to establish a shared secret.  For
 * versions aesgcm and aesgcm128, if a private key is not provided, the ECDH key pair used to encrypt is looked up using |params.keymap[params.keyid]|, and
 * |params.keymap| Key pairs can be created using SJCL.
 * 
 * @param  {string} plaintext	plain text message to encrypt 
 * @param  {Object} params		input parameters
 * 
 * @return {string} encrypted 	hex encoded string of the encrypted message    
 */
function encrypt( plaintext, params )
{
	let header = parseParams( params );

	let result = "";
	
	if ( header.version === 'aes128gcm' )
	{
		// Save the DH public key in the header.
		if ( header.privateKey && !header.keyid )
		{
			header.keyid = header.privateKey.getPublicKey();
		}
		result = writeHeader( header );
	}
	else
	{
		// No header on other versions
		result = "";
	}

	let keyNoncePair = deriveKeyAndNonce( header );
	let plaintextBytes = new Bytes( plaintext );
	
	if( plaintextBytes.length > 4096 )
	{
		Logger.getLogger( "[ece.js]" ).error( 'Unable to encrypt messages longer than 4K bytes. Your message length:{0}', plaintextBytes.length );
	}
	let encrypted = encryptRecord( 
			keyNoncePair.key, 
			keyNoncePair.nonce, 
			plaintext );
	
	return encrypted;
}