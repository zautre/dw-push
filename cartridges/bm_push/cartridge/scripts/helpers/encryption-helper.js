'use strict';
/**
 * This module provides helper functions for encryption.
 *
 * @module helpers/encryption-helper
 */
let SecureRandom = require( 'dw/crypto/SecureRandom' );
let Logger = require( 'dw/system/Logger' );
let Bytes = require( 'dw/util/Bytes' );
let Encoding = require( 'dw/crypto/Encoding' );
let	Transaction = require( 'dw/system/Transaction' );
let	Site = require( 'dw/system/Site' );

let ece = require( '~/cartridge/scripts/helpers/ece.js' );
let sjcl = require( '~/cartridge/scripts/sjcl-1.0.6/sjcl-1.0.6.js' );
let urlBase64 = require( '~/cartridge/scripts/helpers/urlsafe-base64.js' );
let log = Logger.getLogger( "[encryption-helper.js]" );

module.exports = {
	encrypt: encrypt, 
	generateECDHKeys: generateECDHKeys,
	saveECDHKeys: saveECDHKeys,
	generateECDSAKeys: generateECDSAKeys
	
};

/**
 * Computes the shared secret using publicKey as the other party's public key and returns the computed shared secret. 
 *
 * @param  {string} userPublicKey	user EC public key.
 * @param  {string} userAuth		user authentication from the subscription object
 * @param  {string} payload			message payload in plain text
 * 
 * @return {EncryptionObject}   	The encryption object
 */
function encrypt( userPublicKey, userAuth, payload )
{
	let localPublicKey = '';
	let localPrivateKey = '';
	
	_validatePublicKey( userPublicKey );
	_validateAuth( userAuth );

	if( typeof payload !== 'string' )
	{
		log.error( 'Payload must be a string' );
	}

	let random = new SecureRandom();
	
	if( Site.getCurrent().getCustomPreferenceValue( 'reuseECDHKeys' ) )
	{
		localPublicKey = Site.getCurrent().getCustomPreferenceValue( 'ECPublicKey' );
		localPrivateKey = Site.getCurrent().getCustomPreferenceValue( 'ECPrivateKey' );
		log.debug( 'Reusing ECDH ecryption keys.' );
	}
	else 
	{
		let ECKeyPair = generateECDHKeys();
		localPublicKey = ECKeyPair.ECPublicKey;
		localPrivateKey = ECKeyPair.ECPrivateKey;
		log.debug( 'Generating fresh ECDH ecryption keys for each message.' );
	}
	
	let keyMap = {
		'webpushKey' : {
			publicKeySerialized : localPublicKey,
			privateKeySerialized : localPrivateKey,			
			getPublicKey : function getPublicKey(){
				return this.publicKeySerialized;
			},
			getPrivateKey : function getPrivateKey(){
				return this.privateKeySerialized;
			}
		}				
	};
	let keyLabels = {
		'webpushKey' : 'P-256'
	}
	let salt = urlBase64.encode( random.nextBytes( 16 ) );
	let cipherText = ece.encrypt( payload,
		{
			keyid: 'webpushKey',
			keymap : keyMap,
			keylabels : keyLabels,
			dh : userPublicKey,
			salt : salt,
			authSecret : userAuth,
			padSize : 2
		} );
	
	/**
	 * @typedef {Object}  EncryptionObject
	 * @property {string} localPublicKey Base64 encoded EC Local Public Key
	 * @property {string} salt Base64 encoded salt
	 * @property {string} cipherText hex encoded encrypted text
	 */
	return {
		localPublicKey: urlBase64.toURLSave( localPublicKey ),		
		salt: salt,
		cipherText: cipherText
	};
}

/**
 * Validates user's public key. 
 *
 * @param  {string} userPublicKey	user EC public key.
 * @private
 * 
 * @return {void}
 */
function _validatePublicKey( userPublicKey )
{
	if( !userPublicKey )
	{
		log.error( 'No user public key provided for encryption.' );
	}

	if( typeof userPublicKey !== 'string' )
	{
		log.error( 'The subscription p256dh value must be a string.' );
	}

	if( urlBase64.decode( userPublicKey ).length !== 65 )
	{
		log.error( 'The subscription p256dh value should be 65 bytes long.' );
	}
}

/**
 * Validates user's auth. 
 *
 * @param  {string} userAuth	user authentication from the subscription object
 * @private
 * 
 * @return {void}
 */
function _validateAuth( userAuth )
{
	if( !userAuth )
	{
		log.error( 'No user auth provided for encryption.' );
	}

	if( typeof userAuth !== 'string' )
	{
		log.error( 'The subscription auth key must be a string.' );
	}

	if( urlBase64.decode( userAuth ).length < 16 )
	{
		log.error( 'The subscription auth key should be at least 16 bytes long' );
	}
}

/**
 * Generate ECDH public and private key pair
 * 
 * @return {ECKeyPair}    	EC key pair object
 */
function generateECDHKeys()
{
	let random = new SecureRandom();
	
	//seeding SJCL pseudo-random number generator
	let estimatedEntropy = random.nextBytes( 1024 / 8 ); // 128 bytes
	sjcl.random.addEntropy( estimatedEntropy.toString(), 1024, "dw/crypto/SecureRandom" );
	
	// generate ECDH keys  
	let keyPair = sjcl.ecc.elGamal.generateKeys( 256 ); 
	let pub = keyPair.pub.get();
	let sec = keyPair.sec.get();
	
	// Serialize public key and add leading Byte corresponding to uncompressed public key:
	let pubSerialized = sjcl.codec.base64.fromBits( sjcl.bitArray.concat( sjcl.codec.hex.toBits( "0x04" ), pub.x.concat( pub.y ) ) );
	// Serialize private key:
	let secSerialized = sjcl.codec.base64.fromBits( sec );
	
	log.debug( 'EC Private Key base64 string: {0}', secSerialized );
	log.debug( 'EC Public Key base64 string: {0}', pubSerialized );
	
	/**
	 * @typedef {Object}  ECKeyPair 
	 * @property {string} ECPublicKey Base64 encoded EC Public Key
	 * @property {string} ECPrivateKey Base64 encoded EC Private Key
	 */
	return {
		ECPublicKey: pubSerialized,
		ECPrivateKey: secSerialized
	};
}

/**
 * Generate ECDSA public and private key pair, this key pair should only be used for signing
 * 
 * @return {ECDSAKeyPair}    	ECDSA key pair object
 */
function generateECDSAKeys()
{
	let random = new SecureRandom();
	
	//seeding SJCL pseudo-random number generator
	let estimatedEntropy = random.nextBytes( 1024 / 8 ); // 128 bytes
	sjcl.random.addEntropy( estimatedEntropy.toString(), 1024, "dw/crypto/SecureRandom" );
	
	// generate ECDSA keys  
	let keyPair = sjcl.ecc.ecdsa.generateKeys( 256 ); 
	let pub = keyPair.pub.get();
	let sec = keyPair.sec.get();
	
	//adding leading byte for uncompressed public key
	let prefixByte = sjcl.codec.hex.toBits( "0x04" ); 
	let pubSerialized = sjcl.codec.base64.fromBits( sjcl.bitArray.concat( prefixByte, pub.x.concat( pub.y ) ) );
	// Serialize private key:
	let secSerialized = sjcl.codec.base64.fromBits( sec );
	
	log.debug( 'ECDSA Private Key base64 string: {0}', secSerialized );
	log.debug( 'ECDSA Public Key base64 string: {0}', pubSerialized );
	
	/**
	 * @typedef {Object}  ECDSAKeyPair 
	 * @property {string} ECDSAPublicKey Base64 encoded ECDSA Public Key
	 * @property {string} ECDSAPrivateKey Base64 encoded ECDSA Private Key
	 */
	return {
		ECPublicKey: pubSerialized,
		ECPrivateKey: secSerialized
	};
}

/**
 * Save ECDH key pair to corresponding site preferences
 * 
 * @param  {ECKeyPair} ecKeyPair	ECKeyPair.
 *  
 * @return {void}
 */
function saveECDHKeys( ecKeyPair )
{
	try {
		Transaction.wrap( function()
		{				
			Site.getCurrent().setCustomPreferenceValue( 'ECPublicKey', ecKeyPair.ECPublicKey );
			Site.getCurrent().setCustomPreferenceValue( 'ECPrivateKey', ecKeyPair.ECPrivateKey );
		} );	
		log.debug( 'Saved EC Key pair {0} to site preference', JSON.stringify( ecKeyPair ) );
	} catch ( e ) {
		log.error( "Could not save ECKeyPair.{0}", e.message );
	}
}