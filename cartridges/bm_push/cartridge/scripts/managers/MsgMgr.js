'use strict';

module.exports = MsgMgr;

/**
 * Class MsgMgr - This class contains functions for use with messages
 *
 * @class
 */
function MsgMgr()
{
	let SVC = require( 'dw/svc' );
	let Logger = require( 'dw/system/Logger' );
	let Message = require( '~/cartridge/scripts/objects/Message.ds' );
	let FCMResponseObj = require( '~/cartridge/scripts/objects/FCMResponseObj.ds' );
	let Bytes = require( 'dw/util/Bytes' );

	let vapidHelper = require( '~/cartridge/scripts/helpers/vapid-helper.js' );
	let encryptionHelper = require( '~/cartridge/scripts/helpers/encryption-helper.js' );
	let urlBase64 = require( '~/cartridge/scripts/helpers/urlsafe-base64.js' );
	let log = Logger.getLogger( 'MsgMgr.js' );

	// Default TTL is four weeks.
	let DEFAULT_TTL = 2419200;
	let vapidDetails = {};
	let gcmAPIKey = "";


	return {
		sendFCM: sendFCM,
		createMsgObj: createMsgObj,
		preparePayloadFCM: preparePayloadFCM, 
		setVapidDetails: setVapidDetails, 
		sendNotification: sendNotification, 
		generateRequestDetails: generateRequestDetails, 
		setGCMAPIKey: setGCMAPIKey
	};

	/**
	 * Send message using Firebase Cloud Messaging service
	 *
	 * @param {Object} MessageObj Mandatory param for message object
	 *
	 * @returns {dw.system.Response} response Response object
	 */
	function sendFCM( MessageObj )
	{
		let service = "";
		let response = "";
		let endpointParam = {};

		try
		{
			service = SVC.ServiceRegistry.get( "FirebaseCloudMessaging" );
			endpointParam = {
				"payload": MessageObj.Payload
			};
			response = service.call( endpointParam );

			if( response === null || service === null || response.error )
			{
				return null;
			}

			return new FCMResponseObj( response.object );
		}
		catch ( e )
		{
			log.error( "Send to Firebase Cloud Messaging failed with exception: {0} \nResponse: {1}", e.message, response.errorMessage );
			return null;
		}
	}

	/**
	 * Create message object
	 *
	 * @param {string} 			payload Mandatory param for message payload
	 * @param {dw.util.Date} 	scheduleDate message schedule date
	 * @param {string} 			subscriptionObject subscription object
	 * @param {string} 			status message status
	 * @param {dw.util.Date} 	expirationDate message expiration date
	 *
	 * @returns {Object} MessageObj Message object, null if unsuccessfull
	 */
	function createMsgObj( payload, scheduleDate, subscriptionObject, status, expirationDate )
	{
		let MessageObj = null;

		try
		{
			MessageObj = new Message( payload, scheduleDate, subscriptionObject, status, expirationDate );
			log.debug( "Created message: {0}", JSON.stringify( MessageObj ) );
		}
		catch ( e )
		{
			log.error( "Could not create message. Exception: {0}", e.message );
		}

		return MessageObj;
	}

	/**
	 * Prepare the message payload for sending using Firebase Cloud messaging
	 *
	 *	Targets
	 *	@param {string} 			to 							The value must be a registration token, notification key, or topic. Do not set this field when sending to multiple topics. See condition.
	 *	@param {dw.util.ArrayList} 	registration_ids 			This parameter specifies a list of devices (registration tokens, or IDs) receiving a multicast message. It must contain at least 1 and at most 1000 registration tokens.
	 *	@param {string} 			condition   				This parameter specifies a logical expression of conditions that determine the message target.
	 *
	 *	Options
	 *	@param {string} 			collapse_key  				This parameter identifies a group of messages (e.g., with collapse_key: "Updates Available") that can be collapsed, so that only the last message gets sent when delivery can be resumed. This is intended to avoid sending too many of the same messages when the device comes back online or becomes active.
	 *	@param {string} 			priority 					Sets the priority of the message. Valid values are "normal" and "high." On iOS, these correspond to APNs priorities 5 and 10.
	 *	@param {string} 			content_available 			On iOS, use this field to represent content-available in the APNs payload. When a notification or message is sent and this is set to true, an inactive client app is awoken. On Android, data messages wake the app by default. On Chrome, currently not supported.
	 *	@param {string} 			time_to_live 				This parameter specifies how long (in seconds) the message should be kept in FCM storage if the device is offline. The maximum time to live supported is 4 weeks, and the default value is 4 weeks.
	 *	@param {string}				restricted_package_name 	This parameter specifies the package name of the application where the registration tokens must match in order to receive the message.
	 *	@param {string}				dry_run 					This parameter, when set to true, allows developers to test a request without actually sending a message.
	 *
	 *	Payload
	 *	@param {string}				data 						This parameter specifies the custom key-value pairs of the message's payload.For example, with data:{"score":"3x1"}:
	 *	@param {string}				notification 				This parameter specifies the predefined, user-visible key-value pairs of the notification payload. See Notification payload support for detail. For more information about notification message and data message options,
	 *
	 *  @returns {String} 			requestData 				message payload JSON string, null if unsuccessfull
	 */
	function preparePayloadFCM( to, registration_ids, condition, collapse_key, priority, content_available, time_to_live, restricted_package_name, dry_run, data, notification )
	{
		let requestData = null;
		let dataObj = new Object();

		try
		{
			if( to !== null )
			{
				dataObj.to = to;
			}
			if( registration_ids !== null )
			{
				dataObj.registration_ids = registration_ids;
			}
			if( condition !== null )
			{
				dataObj.condition = condition;
			}
			if( collapse_key !== null )
			{
				dataObj.collapse_key = collapse_key;
			}
			if( priority !== null )
			{
				dataObj.priority = priority;
			}
			if( content_available !== null )
			{
				dataObj.content_available = content_available;
			}
			if( time_to_live !== null )
			{
				dataObj.time_to_live = time_to_live;
			}
			if( restricted_package_name !== null )
			{
				dataObj.restricted_package_name = restricted_package_name;
			}
			if( dry_run !== null )
			{
				dataObj.dry_run = dry_run;
			}
			if( data !== null )
			{
				let dataPayload = new Object();
				dataPayload.payload = data;

				dataObj.data = dataPayload;
			}
			if( notification !== null )
			{
				dataObj.notification = notification;
			}

			requestData = JSON.stringify( dataObj );
		}
		catch ( e )
		{
			log.error( "Could not create JSON: Exception: {0}", e.message );
		}

		return requestData;
	}
	
	/**
	 * When sending messages to a GCM endpoint you need to set the GCM API key by either calling setGMAPIKey() or passing in the API key as an option
	 * to sendNotification().
	 * @param  {string} apiKey The API key to send with the GCM request.
	 * 
	 * @returns {void}
	 */
	function setGCMAPIKey( apiKey )
	{
		if( apiKey === null )
		{
			gcmAPIKey = null;
			return;
		}

		if( typeof apiKey === 'undefined' || typeof apiKey !== 'string' || apiKey.length === 0 )
		{
			throw new Error( 'The GCM API Key should be a non-empty string or null.' );
		}
		gcmAPIKey = apiKey;
	}
	
	/**
	 * Upon making requests where you want to define VAPID details, call this method before sendNotification() or pass in the details and options to sendNotification.
	 * @param  {string} subject    This must be either a URL or a 'mailto:' address. For example: 'https://my-site.com/contact' or 'mailto: contact@my-site.com'
	 * @param  {string} publicKey  The public VAPID key.
	 * @param  {string} privateKey The private VAPID key.
	 * 
	 * @return {VapidDetails} vapidDetails VAPID Details Object
	 */
	function setVapidDetails( subject, publicKey, privateKey )
	{
		if( arguments.length === 1 && arguments[0] === null )
		{
			vapidDetails = null;
			return;
		}

		vapidHelper.validateSubject( subject );
		vapidHelper.validatePublicKey( publicKey );
		vapidHelper.validatePrivateKey( privateKey );

		/**
		 * @typedef {Object}  VapidDetails
		 * @property {string} subject VAPID subject
		 * @property {string} publicKey VAPID publicKey
		 * @property {string} privateKey VAPID privateKey
		 */
		vapidDetails = {
			subject: subject,
			publicKey: publicKey,
			privateKey: privateKey
		};
	}
	
	/**
	 * To get the details of a request to trigger a push message, without sending a push notification call this method.
	 *
	 * This method will log an error if there is an issue with the input.
	 * @param  {Object} [subscription]		The PushSubscription you wish to send the notification to.
	 * @param  {string} [payload]           The payload you wish to send to the user.
	 * @param  {Object} [options]           Option vapid keys can be passed in if they are unique for each notification you wish to send.
	 * 
	 * @return {Object}                     This method returns an Object which contains 'endpoint', 'method', 'headers' and 'payload'.
	 */
	function generateRequestDetails( subscription, payload, options )
	{
		if( !subscription || !subscription.endpoint )
		{
			log.error( 'You must pass in a subscription with at least an endpoint.' );
		}

		if( typeof subscription.endpoint !== 'string' || subscription.endpoint.length === 0 )
		{
			log.error( 'The subscription endpoint must be a string with a valid URL.' );
		}

		if( payload )
		{
			// Validate the subscription keys
			if( !subscription.keys || !subscription.keys.p256dh || !subscription.keys.auth )
			{
				log.error( 'To send a message with a payload, the subscription must have \'auth\' and \'p256dh\' keys.' );
			}
		}

		let currentGCMAPIKey = gcmAPIKey;
		let currentVapidDetails = vapidDetails;
		let timeToLive = DEFAULT_TTL;
		let extraHeaders = {};

		if( options )
		{
			let validOptionKeys = [
				'headers',
				'vapidDetails',
				'TTL'
			];
			let optionKeys = Object.keys( options );
			for( let i = 0; i < optionKeys.length; i += 1 )
			{
				let optionKey = optionKeys[i];
				if ( validOptionKeys.indexOf( optionKey ) === -1 )
				{
					log.error( '\'' + optionKey + '\' is an invalid option. ' +
						'The valid options are [\'' + validOptionKeys.join( '\', \'' ) +
						'\'].' );
				}
			}

			if( options.headers )
			{
				extraHeaders = options.headers;
				let duplicates = Object.keys( extraHeaders )
					.filter( function( header )
					{
						return typeof options[header] !== 'undefined';
					} );

				if( duplicates.length > 0 )
				{
					log.error( 'Duplicated headers defined [' +
						duplicates.join( ',' ) + ']. Please either define the header in the top level options OR in the \'headers\' key.' );
				}
			}

			if( options.vapidDetails )
			{
				currentVapidDetails = options.vapidDetails;
			}

			if( options.TTL )
			{
				timeToLive = options.TTL;
			}
		}

		if( typeof timeToLive === 'undefined' )
		{
			timeToLive = DEFAULT_TTL;
		}

		let requestDetails = {
			method: 'POST',
			headers:
			{
				TTL: timeToLive
			}
		};
		Object.keys( extraHeaders ).forEach( function( header )
		{
			requestDetails.headers[header] = extraHeaders[header];
		} );
		let requestPayload = null;

		if( payload )
		{
			if( !subscription.keys || typeof subscription !== 'object' || !subscription.keys.p256dh || !subscription.keys.auth )
			{
				log.error( 'Unable to send a message with payload to this subscription since it doesn\'t have the required encryption keys' );
			}

			let encrypted = encryptionHelper.encrypt( subscription.keys.p256dh, subscription.keys.auth, payload );

			requestDetails.headers['Content-Length'] 	= encrypted.cipherText.length;
			requestDetails.headers['Content-Type'] 		= 'application/octet-stream';
			requestDetails.headers['Content-Encoding'] 	= 'aesgcm';
			requestDetails.headers.Encryption 			= 'salt=' + encrypted.salt;
			requestDetails.headers['Crypto-Key'] 		= 'dh=' + encrypted.localPublicKey;

			requestPayload = encrypted.cipherText;
		}
		else
		{
			requestDetails.headers['Content-Length'] = 0;
		}

		let isGCM = subscription.endpoint.indexOf( 'https://android.googleapis.com/gcm/send' ) === 0;
		// VAPID isn't supported by GCM hence the if, else if.
		if( isGCM )
		{
			if( !currentGCMAPIKey )
			{
				log.warn( 'Attempt to send push notification to GCM endpoint, ' +
					'but no GCM key is defined. Please use setGCMApiKey() or add ' +
					'\'gcmAPIKey\' as an option.' );
			}
			else
			{
				requestDetails.headers.Authorization = 'key=' + currentGCMAPIKey;
			}
		}
		else if( currentVapidDetails )
		{
			let audience = subscription.endpoint.split( ".com" )[0] + ".com";
	
			let vapidHeaders = vapidHelper.getVapidHeaders(
				audience,
				currentVapidDetails.subject,
				currentVapidDetails.publicKey,
				currentVapidDetails.privateKey
			);
	
			requestDetails.headers.Authorization = vapidHeaders.Authorization;
			if( requestDetails.headers['Crypto-Key'] )
			{
				requestDetails.headers['Crypto-Key'] += ';' +
					vapidHeaders['Crypto-Key'];
			}
			else
			{
				requestDetails.headers['Crypto-Key'] = vapidHeaders['Crypto-Key'];
			}
		}
	    
		requestDetails.body = requestPayload;
		requestDetails.endpoint = subscription.endpoint;

		return requestDetails;
	}

	/**
	 * To send a push notification call this method with a subscription, optional payload and any options.
	 * 
	 * @param  {Object} 			subscription 	The PushSubscription you wish to send the notification to.
	 * @param  {string} 			[payload]       The payload you wish to send to the the user.
	 * @param  {Object} 			[options]       Option for the VAPID keys can be passed in if they are unique for each notification you wish to send.
	 * 
	 * @return {void}                       		
	 */
	function sendNotification( subscription, payload, options )
	{
		let service = "";
		let response = "";
		let endpointParam = "";
		let requestDetails = "";

		try
		{
			requestDetails = this.generateRequestDetails( subscription, payload, options );
			log.debug( 'Request generated: {0}', JSON.stringify( requestDetails ) );
		}
		catch ( err )
		{
			log.error( 'Unable to generate request details for subscription {0}, payload {1}, options {2}. Error {3}', subscription, payload, options, err.message );
		}

		try
		{
			service = SVC.ServiceRegistry.get( "PushNotificationService" );
			endpointParam = {
				"endpoint": requestDetails.endpoint,
				"payload": requestDetails.body,
				"headers": requestDetails.headers
			};
			response = service.call( endpointParam );
							
			if ( response === null || response.error )
			{
				log.error( "Send to {0} failed with error message: {1}", requestDetails.endpoint, response.errorMessage );
				
			}
			log.debug( 'Successfuly send message {0}, response: {1}', JSON.stringify( requestDetails ), JSON.stringify( response ) );
			//return new FCMResponseObj( response.object );
		}
		catch ( e )
		{
			log.error( "Send to {0} failed with exception: {1} \nResponse: {2}", requestDetails.endpoint, e.message, response.errorMessage );
			
		}
	}
}