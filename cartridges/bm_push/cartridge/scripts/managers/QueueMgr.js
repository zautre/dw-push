'use strict';

module.exports = QueueMgr;

/**
 * Class QueueMgr - This class contains functions for use with message queue
 *
 * @class
 */
function QueueMgr()
{
	let	Transaction = require( 'dw/system/Transaction' );
	let Calendar = require( 'dw/util/Calendar' );
	let UUIDUtils = require( 'dw/util/UUIDUtils' );
	let Object = require( 'dw/object' );
	let Logger = require( 'dw/system/Logger' );
	let CustomObjectMgr = require( 'dw/object/CustomObjectMgr' );
	let Site = require('dw/system/Site');
	
	let CO_TYPE = "push_dataObject";
	let log = Logger.getLogger( 'QueueMgr.js' );
    
	return {
		enqueue : enqueue,
		dequeue : dequeue,
		getMessage : getMessage,
		getAllMessages : getAllMessages,
		cleanup : cleanup,
		failMsg : failMsg,
		edit : edit, 
		updateStatus : updateStatus,
		deleteAll : deleteAll
	};

	/**
	 * Enqueue message object to message queue
	 *
	 * @param {Object} MessageObj Mandatory param for message object
	 *
	 * @returns {String} QueueID Unique queue ID, null if enqueue is not successful
	 */
	function enqueue( MessageObj )
	{		
		let transactionUID = UUIDUtils.createUUID();
		
		try {
			Transaction.wrap( function()
			{				
				let dataObject = CustomObjectMgr.createCustomObject( CO_TYPE, transactionUID );		
				dataObject.custom.data = MessageObj.Payload;
				dataObject.custom.scheduleDate = MessageObj.ScheduleDate;
				dataObject.custom.subscriptionObject = MessageObj.SubscriptionObject;
				dataObject.custom.provider = JSON.parse( MessageObj.SubscriptionObject ).provider;
				dataObject.custom.status = MessageObj.Status;
				dataObject.custom.expirationDate = MessageObj.ExpirationDate;
			} );
			log.debug( "Enqueue successful: \n transactionUID: {0},\n msgObj: {1}", transactionUID, JSON.stringify( MessageObj ) );
		} catch ( e ) {
			log.error( "Could not enqueue message with:\n payload {0},\n scheduleDate: {1},\n subscriptionObject: {2}.\n Exception message: {3}", MessageObj.Payload, MessageObj.ScheduleDate, MessageObj.SubscriptionObject, e.message );
			return null;
		}	
		
		return transactionUID;
	}
	
	/**
	 * Remove (dequeue) message object from message queue
	 *
	 * @param {String} QueueID Queue ID of the message
	 *
	 * @returns {Boolean} True if dequeue has been successful, false otherwise
	 */
	function dequeue( QueueID )
	{		
		let isDequeueSuccessful = false;
		
		try {							
			let dataObject = getMessage( QueueID );
			if( dataObject === null ) {
				log.error( "Could not find message with:\n QueueID {0}.", QueueID );
			} else 
			{
				Transaction.wrap( function()
				{
					CustomObjectMgr.remove( dataObject );
				} );
				isDequeueSuccessful = true;
				log.debug( "Dequeue successful: MsgObject {0}", dataObject.toString() );
			}				
		} catch ( e ) {
			log.error( "Could not remove (dequeue) message with:\n QueueID {0}.\n Exception message: {1}", QueueID, e.message );
		}	
		
		return isDequeueSuccessful;
	}
	
	/**
	 * Get message object by queue ID
	 *
	 * @param {String} QueueID Queue ID of the message
	 *
	 * @returns {Object} MessageObj Message object
	 */
	function getMessage( QueueID )
	{
		let dataObject = CustomObjectMgr.getCustomObject( CO_TYPE, QueueID );
		
		return dataObject;
	}
	
	/**
	 * Get all messages in the queue
	 * 
	 * @returns {dw.util.SeekableIterator} MessageObjs Message objects currently in the queue. 
	 * It is strongly recommended to call close() on the returned SeekableIterator
	 */
	function getAllMessages()
	{
		let seekableIterator = CustomObjectMgr.getAllCustomObjects( CO_TYPE );
		
		return seekableIterator;
	}
	
	/**
	 * Clean up data queue from messages in statuses: 'expired', 'failed' and 'sent'	 
	 * 
	 * @returns {void}  
	 */
	function cleanup()
	{	
		let messageObjects = CustomObjectMgr.queryCustomObjects( CO_TYPE, "custom.status = 'expired' OR custom.status = 'failed' OR custom.status = 'sent'", null );
		
		try {
			Transaction.wrap( function()
			{		
				while( messageObjects.hasNext() ) {
					let object = messageObjects.next();
					log.debug( "Prepared to delete: {0},\n subscriptionObject: {1},\n payload: {2},\n status: {3},\n scheduleDate: {4},\n expirationDate: {5}", object.toString(), object.custom.subscriptionObject, object.custom.data, object.custom.status, object.custom.scheduleDate, object.custom.expirationDate );
				
					CustomObjectMgr.remove( object );
				}	
			} );
			log.debug( "Queue cleanup successful" );
		} catch ( e ) {
			log.error( "Could not cleanup messages. Exception: {0}", e.message );
			throw Error( "Could not cleanup messages. Exception: {0}", e.message );
		}

		messageObjects.close();
	}
	
	/**
	 * Fail message
	 *
	 * @param {String} QueueID Queue ID of the message
	 *
	 * @returns {void}
	 */
	function failMsg( QueueID )
	{		
		try {							
			let dataObject = getMessage( QueueID );
			if ( dataObject === null ) {
				log.error( "Could not find message with:\n QueueID {0}.", QueueID );
			} else {
				Transaction.wrap( function()
				{
					dataObject.custom.status = "failed";
				} );
			}				
		} catch ( e ) {
			log.error( "Could not fail message with:\n QueueID {0}.\n Exception message: {1}", QueueID, e.message );
		}
	}
	
	/**
	 * Edit message object
	 *
	 * @param {String} QueueID Queue ID of the message
	 * @param {String} editedMessageObj edited message object
	 *
	 * @returns {Boolean} result true if edit succeeds, false otherwise
	 */
	function edit( QueueID, editedMessageObj )
	{
		let isEditSuccessful = false;
		try {							
			let dataObject = getMessage( QueueID );
			if ( dataObject === null ) {
				log.error( "Could not find message with:\n QueueID {0}.", QueueID );
			} else {
				Transaction.wrap( function()
				{
					dataObject.custom.data = editedMessageObj.Payload;
					dataObject.custom.scheduleDate = editedMessageObj.ScheduleDate;	
					dataObject.custom.subscriptionObject = editedMessageObj.SubscriptionObject;	
					dataObject.custom.status = editedMessageObj.Status;
					dataObject.custom.expirationDate = editedMessageObj.ExpirationDate;
				} );
				isEditSuccessful = true;
				log.debug( "Edit message with queue id {0} successful", QueueID );
			}				
		} catch ( e ) {
			log.error( "Could not edit message with:\n QueueID {0}.\n Exception message: {1}", QueueID, e.message );
		}
		
		return isEditSuccessful;
	}
	
	/**
	 * Update message status
	 *
	 * @param {String} QueueID Queue ID of the message
	 * @param {String} NewStatus The new status of the message
	 *
	 * @returns {void}
	 */
	function updateStatus( QueueID, NewStatus )
	{		
		try {							
			let dataObject = getMessage( QueueID );
			if ( dataObject === null ) {
				log.error( "Could not find message with:\n QueueID {0}.", QueueID );
			} else {
				Transaction.wrap( function()
				{
					dataObject.custom.status = NewStatus;
				} );
				log.debug( "Message with queue id {0} status updated to {1}", QueueID, NewStatus );
			}				
		} catch ( e ) {
			log.error( "Could not update message with:\n QueueID {0}.\n Exception message: {1}", QueueID, e.message );
		}
	}
	
	
	/**
	 * Deletes all messages from the queue
	 * 
	 * @return {void}
	 */
	function deleteAll( )
	{
		try {
			Transaction.wrap( function()
			{				
				let seekableIterator = CustomObjectMgr.getAllCustomObjects( CO_TYPE );
				while( seekableIterator.hasNext() ) {
					let object = seekableIterator.next();
					log.debug( "Prepared to delete: {0},\n subscriptionObject: {1},\n payload: {2},\n status: {3},\n scheduleDate: {4},\n expirationDate: {5}", object.toString(), object.custom.subscriptionObject, object.custom.data, object.custom.status, object.custom.scheduleDate, object.custom.expirationDate );
				
					CustomObjectMgr.remove( object );
				}
				seekableIterator.close();
			} );	
		} catch ( e ) {
			log.error( 'Could not empty the message queue. Exception: {0}', e.message );
		}
	}
}